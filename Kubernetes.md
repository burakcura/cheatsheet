# Kubernetes
Container orchestration
- [Commands](#commands)
- [Deployment by Auto-Generated Configuration File](##deployment-by-auto-generated-configuration-file)
- [Deployments by Configuration File](#deployments-by-configuration-file)
- [Secret](#secret)
- [Namespaces](#namespaces)
- [Ingres](#ingres)
- [Helm](#helm)

## Strcuture
Deployment -> Replicaset -> Pods -> Containers
- Deployment is a blueprint of pod

## Commands
### Get all pod info and replicaset info
```
kubectl get service
kubectl get pod (-o wide for more details)
kubectl get replicaset
kubectl get deployment <name> -o yaml
```
### Get logs
```
kubectl logs <pod_name>
```
### Exec a container in a pod (use exit for exit)
```
kubectl exec -it <pod_name> -- bin/bash
```

## Deployment by Auto-Generated Configuration File
### Create auto-generated config file (yaml)
```
kubectl create deployment <name> --image <image_name> --replicas=3 --port=3000
```
### Edit deployment config file
```
kubectl edit deployment <name>
```
### Delete deployment
```
kubectl delete deployment <name>
```

## Deployments by Configuration File
### Create a deployment or service by using a config file (yaml)
```
kubectl apply -f <config_file.yaml>
```
### Delete a deployment or service by using a config file (yaml)
```
kubectl delete -f <config_file.yaml>
```
### Deployment and Service Config File Yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: example-deployment
  namespace: mynamespace
  labels:
    app: example
spec:
  replicas: 3
  selector:
    matchLabels:
      app: example
  template:
    metadata:
      labels:
        app: example
    spec:
      containers:
      - name: example
        image: example:lastest
        resources:
          limits:
            cpu: 4000m
          requests:
            cpu: 200m
        ports:
        - containerPort: 5000
        env:
        - name: DB_URL
          value: "postgresql://xxx/mydb"
---      
apiVersion: v1
kind: Service
metadata:
  name: example-service
spec:
  selector:
    app: example
  type: LoadBalancer
  ports:
    - protocol: TCP
      port: 4000
      targetPort: 5000
      nodePort: 30000
  externalIPs:
    - 80.11.12.10
```
Parts of yaml file:
- metadata: identify the object (name, UID,namespace)
- specs: desired behavior of the pod
- status: auto added and updated

Service types:
- ClusterIP: Internal port passing (default) 
- NodePort: Exposes the Service on each Node's IP at a static port. Uses ClusterIP.
- LoadBalancer: Exposes the Service externally using a cloud provider's load balancer. Use ClusterIP and NodePort.
- ExternalName:  Maps the Service to a url path

## Secret
Secret is not encrypted so Kubernetes EncryptionConfiguration or third party tools such as AWS Secrets Manager, Google Cloud Platform KMS are suggested
```
apiVersion: v1
kind: Secret
metadata:
    name: db-secret
type: Opaque
data:
    db-username: dXNlcm5hbWU=
    db-password: cGFzc3dvcmQ=
```

To read secrects use `valueFrom` and `secretKeyRef`
```
env:
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: db-secret
      key: db-password
```

### Get random secret string on Linux
```
head -c 10 /dev/urandom | base64
```

## Namespaces
Why namespace:
- For organization of complex clusters (Monitorig, Database, Elastic Stack, Microservices)
- For Multi-team usage
- Deployment/Development
- Different Versions
- To limit access of users

Limits:
- Access other services but not other config map, secrets
- Volumes are accesable from all namespaces

Create new namespace
```
kubectl crate namespace <namespace_name>
```

Get config map (default namespace=defult):
```
kubectl get configmap -n <namespace_name>
```

Apply file for a namespace:
```
kubectl apply -f <config_file.yaml> --namespace=<namespace_name>
```
But better to add `namespace:<namespace_name>` under metadata in config file yaml to make it more documented and more convenient

## Ingres
Routing for multiple paths or sub-domains
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: myapp-ingress
  namespace: my-namespace
spec:
  rules:
  - host: myapp.com
    http:
      paths: 
      - path: check/
        backend:
          serviceName: myapp
          servicePort: 3000
```
For https use tls under specs and add secretName for tls certificate

## Helm
Use existing heml charts (Yaml bundles) such as 
- Elastic stack for logging
- Nginx-ingres
- Prometheus
- Jenkins for CI/CD

Custom helm charts with template values (storable in heml repos)

### Templates in helm
Practical for CI/CD usage, redeploying development/production, helm charts 
- Values can be used by `{{.Values.name}}` and `{{.Values.container.image}}`
- Values can be given by values.yaml file or command line `--set`

values.yaml:
```
name: myapp
container:
  name: myapp-container
  image: myapp-image
```

For databases -> statefulset
- Deployment of Statefulset is not easy
- DB are often hosted outside of K8s
